from asyncio import tasks
from django.contrib.auth.decorators import login_required
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from rest_framework.response import Response 
from rest_framework.decorators import api_view
from .models import Task
from app1.models import Client,User,Incentives,Payment
from datetime import date,timedelta
import datetime
from pathlib import Path
from django.utils.dateparse import parse_date
import calendar
from django.contrib.auth import login,logout,authenticate

def create_task(request):
    today = date.today().strftime("%Y-%m-%d")
    user= request.user
    k = Task.objects.filter(date=today,employee=user)
    l= Client.objects.all()
    if request.method == 'POST':
        pro = request.POST.get('project')
        p = Client.objects.get(id=pro)
        add_date = request.POST.get("date")        
        dayname = datetime.datetime.strptime(add_date, "%Y-%m-%d")
        qq = dayname.strftime('%d-%m-%Y')
        user= request.user
        taskdetails = request.POST.get('taskdetails')
        hours = request.POST.get('hours')
        amount=user.amount*int(hours)
        today = date.today().strftime("%Y-%m-%d")
        day = dayname.strftime("%A")
        count=0
        remainingHours = int(8-(int(count)+int(hours)))
        if day == 'Sunday':
            return render (request,'listtask.html',{'k':k,'l':l,'rh':0,'msg':'sorry hours have exceded','sun':'Sorry Sunday is a Holiday' })
        if user.task_set.filter(date=add_date):
            k = Task.objects.filter(date=add_date,employee=user)
            for i in user.task_set.filter(date=add_date):
                count+= int(i.hours)
                remainingHours = int(8-(int(count)+int(hours)))
            if not int(count)<8 or not int(count)+int(hours)<=8:
                k = Task.objects.filter(date = add_date,employee = user)
                return render (request,'listtask.html',{'k':k,'l':l,'msg':'sorry hours is exceded','rh':0,'selectedDate':add_date })
        Task.objects.create(employee=user,date=add_date,day=day,project= p,taskdetails=taskdetails, hours=hours,amount=amount,remainingHours=remainingHours)
        request.session['search_date'] = add_date
        return redirect('listtask')
    return redirect('listtask')
def listtask(request):
    user= request.user
    if user.is_authenticated :
        message=False
        d=0
        if not 'search_date' in request.session:
            search_date = date.today().strftime("%Y-%m-%d")
        else:
            search_date = request.session.get('search_date')
            del request.session['search_date']
        m= search_date
        if request.method=='POST':
            search_date=request.POST.get('searchdate')
        search_date_f = datetime.datetime.strptime(search_date,"%Y-%m-%d").date()
        date_range = date.today()-timedelta(days=3)
        date_end = date.today()+timedelta(days=1)
        if search_date_f < date_range or search_date_f > date.today():
            message="Sorry, you are not allowed to add task on this date"
            d="1"
        k=user.task_set.filter(date=search_date)
        count = 0
        if k:
            for i in k:
                count+= int(i.hours)
                remainingHours = int(8-int(count))
        else:
            remainingHours = 8
        l= Client.objects.filter(status='Active')  
    else:
        msg = "Your section has expired"
        return render(request,"login.html",{"msg":msg})
    return render (request,'listtask.html',{'k':k,'l':l,'rh':remainingHours,'selectedDate':search_date,'msg':message,'m':m,'d':d})
def deletetask(request,id):
    u=Task.objects.get(id=id)
    u.delete()
    search_date = u.date.strftime("%Y-%m-%d")
    request.session['search_date'] = search_date
    return redirect('listtask')

@login_required
def log(request):
    logout(request)
    return redirect('loginf')

def BillingPerfomance(request):
    user= request.user
    if user.is_authenticated:
        rank = user.rank
        des = user.desigenation
        mail =user.officemail
        userid=user.id
        h=0
        today = date.today().strftime("%Y-%m")
        r = date.today()
        start_date=date(int(r.year),int(r.month),1)
        k=User.objects.get(id=userid)
        print("NAME",k,rank)
        j=User.objects.filter(id=userid)
        for i in j:
            d=i.date_joined
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=start_date,date__lte=r)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                value=0
                Totalvalue=0
                messege="sorry you have no approved task in this month"
                return render(request,'justgageuser.html',{'des':des,'mail':mail,'messege':messege,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
        total_days_in_a_month=25
        total_working_hour_in_a_month=25*8
        numerator=per_hour*total_hour*100
        denominatior=per_hour* total_working_hour_in_a_month          
        value=numerator/denominatior
        if request.method=='POST':
            a=request.POST.get('date')
            print('date',a)
            y=a.split('-')[0]
            m=a.split('-')[1]
            year=int(y)
            month=int(m)
            num_days=(calendar.monthrange(year,month)[1]);
            end_date=date(int(y),int(m),num_days)
            start_date=date(int(y),int(m),1)
            for i in j:
                d=i.date_joined
                s=i.salary
                b=i.billing
                total=int(s)*int(b)
                per_day=total/25
                per_hour=per_day/8
                task=i.task_set.filter(status=True,date__gte=start_date,date__lte=end_date)
                if task:
                    for x in task:
                        h +=x.hours
                        total_hour=h
                    h=0
                else:
                    value=0
                    Totalvalue=0
                    messege="sorry you have no approved task in this month"
                    return render(request,'justgageuser.html',{'des':des,'mail':mail,'messege':messege,'rank':rank,'k':k,'value':value,'id':userid,'Totalvalue':Totalvalue,'searchDate':a})
            total_days_in_a_month=25
            total_working_hour_in_a_month=25*8
            numerator=per_hour*total_hour*100
            denominatior=per_hour* total_working_hour_in_a_month          
            value=numerator/denominatior
            print(" value",value)
            current_date=date.today()
            end_date=date(int(current_date.year),int(current_date.month),1)
            days_before=(end_date-timedelta(days=1))
            num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
            for i in j:
                s=i.salary
                b=i.billing
                total=int(s)*int(b)
                per_day=total/25
                per_hour=per_day/8
                task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
                if task:
                    for x in task:
                        h +=x.hours
                        total_hour=h
                    h=0
                else:
                    Totalvalue=0
                    return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':a})
            numerator=per_hour*total_hour*100
            denominatior=per_hour*num_months*200
            Totalvalue=numerator/denominatior
            print("Totalvalue",Totalvalue)
            return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'value':value,'id':userid,'searchDate':a,'Totalvalue':Totalvalue})
### for total performance ###
        current_date=date.today()
        end_date=date(int(current_date.year),int(current_date.month),1)
        days_before=(end_date-timedelta(days=1))
        num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
        for i in j:
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                Totalvalue=0
                return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
        numerator=per_hour*total_hour*100
        denominatior=per_hour*num_months*200
        Totalvalue=numerator/denominatior
        print("Totalvalue",Totalvalue)
    else:
        msg = "Your section has expired"
        return render(request,"login.html",{"msg":msg})
    return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})

def userincentive(request):
    user = request.user
    if user.is_authenticated:
        userid = user.id
        first_day_of_current_month = date.today().replace(day=1)
        last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
        b=last_day_of_previous_month.strftime('%Y-%m')
        mon=last_day_of_previous_month.strftime('%B')
        year=last_day_of_previous_month.year
        k= Incentives.objects.filter(employee=user,month=mon,year=year)
        if request.method =='POST':
            da = request.POST.get('searchdate')
            y = da.split('-')[0]
            m = da.split('-')[1]
            mo=date(int(y),int(m),1).strftime('%B')
            k=Incentives.objects.filter(employee=user,month=mo,year=y)
            return render(request,'userincentive.html',{'k':k,'selectedDate':da})
    else:
        msg = "Your section has expired"
        return render(request,'login.html',{'msg':msg})
    return render(request,'userincentive.html',{'k':k,'selectedDate':b})

def dashboard(request):
    return render(request,'dashboard.html')

