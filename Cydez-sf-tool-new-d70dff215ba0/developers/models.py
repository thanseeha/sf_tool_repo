from django.db import models
from app1.models import Client,User
from django.conf import settings
from datetime import date
from django.db.models.signals import post_save,pre_delete
from django.dispatch import receiver

class Task(models.Model):
    employee = models.ForeignKey('app1.User',on_delete=models.CASCADE,null=True,blank=True)
    date = models.DateField(null=True,blank=True)
    day = models.CharField(default=" ",max_length=200,null=True,blank=True)
    project = models.ForeignKey('app1.Client',on_delete=models.CASCADE,null=True,blank=True)
    taskdetails = models.CharField(default=" ",max_length=300,null=True,blank=True)
    hours = models.IntegerField(null=True,blank=True,default=0)
    remainingHours = models.IntegerField(null=True,blank=True)
    amount=models.IntegerField(null=True)
    status = models.BooleanField(null=True)
    def __str__(self):
        return str(self.project)

@receiver(pre_delete, sender=Task)
def del_ba(sender, instance=None, created=False, **kwargs):
    if instance.status == True and instance.amount:
        project = instance.project
        if hasattr(project,'estimate'):
            estimate = instance.project.estimate
            estimate.billed_amount -= instance.amount     
            engagement=instance.project.Parent
            engagement.engagementValue
            estimate.save()



