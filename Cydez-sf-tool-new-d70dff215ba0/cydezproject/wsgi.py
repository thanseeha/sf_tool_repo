"""
WSGI config for cydezproject project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os,sys

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cydezproject.settings')
sys.path.append('/var/www/sf_qa/cydez/cydezproject/cydezproject')
sys.path.append('/var/www/sf_qa/cydez/cydezproject')

application = get_wsgi_application()
