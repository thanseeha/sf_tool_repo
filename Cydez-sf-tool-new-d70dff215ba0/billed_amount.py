def add_billed_amount():
	for i in Client.objects.all():
		for task in i.task_set.all():
		     client_task_total += task.amount
		if hasattr(i,'estimate'):
		     i.billed_amount = client_task_total
		     i.save()
		client_task_total = 0

def reset_billed_amount():
	for i in Client.objects.all():
		if hasattr(i,'estimate'):
		     i.billed_amount = 0
		     i.save()
