from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse, request
from django.views.decorators.csrf import csrf_exempt
from .models import User,Estimate,Client,Payment,Incentives,Expenditure,Engagement,Estimation,Productdetails
from developers.models import Task
from django.contrib.auth import login,logout,authenticate
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.core.mail  import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
import random
import pandas as pd 
import os 
import xlsxwriter
from datetime import datetime
from datetime import date
from datetime import timedelta
from pathlib import Path
from django.http import HttpResponseRedirect
from django.utils.dateparse import parse_date
import calendar
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.db.models import Q
from django.template.loader import render_to_string 
from django.template.loader import get_template
from app1.utils import render_to_pdf,topdf,printoncertificate,date_convert
import mimetypes
from io import BytesIO
import apscheduler.schedulers.background
import xlwt
from django.core.mail import EmailMessage
import schedule
import time

#BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = Path(__file__).resolve().parent.parent
def logina(request):
    return render (request,'log.html')
## user login ##
def loginf(request):
    if request.method=='POST':
        phone=request.POST.get('phone')
        password=request.POST.get('password')
        user = authenticate(username=phone, password=password)
        u = User.objects.filter(phone=phone,otp=password,status="Active").exists()
        if u and user is not None:
            r=User.objects.filter(phone=phone,otp=password,status="Active")
            role=r[0].role
            if role == "Employee":
                login(request,user)
                return redirect('listtask')
            elif role == "Consultant":
                userid=r[0].id
                login(request,user)
                return redirect('consultatntbilling',userid=userid)
        else:
            msg = "Wrong attempt. Try again."
            return render(request,'login.html',{'msg':msg})
    return render(request,'login.html')

def adminlogin(request):
    if request.method=='POST':
        username=request.POST.get('name')
        password=request.POST.get('psw')
        if username=="admin@gmail.com" and password=="admin":
            return render(request,'dashboard.html',{'username':username})
        else:
            messages.info(request," Invalid email id and password")
            return render(request,'log.html')
    return render(request,'log.html')

def dashboard(request):
    return render (request,'dashboard.html')

def listUser(request):
    activeusers=list(User.objects.filter(status="Active").exclude(is_superuser=True))
    inactiveusers=list(User.objects.filter(status="Inactive").exclude(is_superuser=True))
    datas_list=activeusers + inactiveusers
    query = request.GET.get('q')
    if query:
        datas_list = User.objects.filter(first_name__contains=query)
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'user.html',{'datas': datas})
    
def addUser(request):
    datas=User.objects.all()
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        email1=request.POST.get('email1')
        salary=request.POST.get('salary')
        billing=request.POST.get('billing')
        status=request.POST.get('status')
        role=request.POST.get('role')
        desiganation=request.POST.get('desiganation')
        rank=request.POST.get('rank')
        if User.objects.filter(phone=phone).exists():
            messege="Phone Number already exists"
            k={'status':status,"role":role,"name":name,'email':email,'salary':salary,'billing':billing,'email1':email1,'desiganation':desiganation,'rank':rank}
            return render(request,'user.html',{'messege':messege,'userdetails':k,'datas':datas})
        otp=random.randint(0000,9999)
        sb=int(salary)*int(billing)
        z=sb/25
        x=z/8
        user=User.objects.create_user(status=status,role=role,first_name=name,phone=phone,email=email,salary=salary,billing=billing,amount=x,otp=otp,password=str(otp),officemail=email1,desigenation=desiganation,rank=rank)
        subject='Cydez Technologies'
        message='Hi your username is {0}  and password is {1} and please login "http://sf.cydeztechnologies.com/" Thank you'.format(user.phone,user.otp)
        username=user.phone
        password=user.otp
        email_from=settings.EMAIL_HOST_USER
        recipient_list=[user.email,user.officemail]
        send_mail(subject,message,email_from,recipient_list)
        return redirect('listUser')
    return render(request,'user.html',{'datas': datas})

def deleteUser(request,userid):
    k=User.objects.get(id=userid)
    k.delete()
    return redirect('listUser')

def editUser(request): 
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        salary=request.POST.get('salary')
        user_id=request.POST.get('user_id')
        billing=request.POST.get('billing')
        email1=request.POST.get('email1')
        desiganation=request.POST.get('desiganation')
        rank=request.POST.get('rank')
        status=request.POST.get('status')
        role=request.POST.get('role')
        sb=int(salary)*int(billing)
        z=sb/25
        x=z/8
        k=User.objects.filter(id=user_id)
        print(k)
        k.update(status=status,role=role,first_name=name,phone=phone,email=email,salary=salary,amount=x,billing=billing,officemail=email1,desigenation=desiganation,rank=rank)
        return redirect('listUser')
## estimation ##
def estimate(request):
    l= Client.objects.all().order_by('clientNameid')
    if request.method=='POST':
        projectname=request.POST.get('projectname')
        p = Client.objects.get(id=projectname)
        estimatedamount=request.POST.get('estimatedamount')
        paidamount=request.POST.get('paidamount')
        dueamount= int(estimatedamount)-int(paidamount)
        # dueamount=request.POST.get('dueamount')
        Estimate.objects.create(projectname=p,estimatedamount=estimatedamount,paidamount=paidamount,dueamount=dueamount)
        return redirect('listProject')
    return render(request,'estimate.html',{'l':l})
def editestimate(request):
    l= Client.objects.all()
    if request.method=='POST':
        estimatedamount=request.POST.get('estimatedamount')
        paidamount=request.POST.get('paidamount')
        dueamount= int(estimatedamount)-int(paidamount)
        user_id=request.POST.get('user_id')
        k=Estimate.objects.filter(id=user_id)
        k.update(estimatedamount=estimatedamount,paidamount=paidamount,dueamount=dueamount)
        return redirect('listProject')

#email clients about the approved amount and billed amount with worker details
def auto_email_friday():
    dayf=date.today() - timedelta(days=5)
    dayt=dayf-timedelta(days=7)
    task_filter_bydate=Task.objects.filter(date__gte=dayt,date__lte=dayf,status=True)
    pro_list=[]
    for i in task_filter_bydate:
        if i.project_id not in pro_list:
            pro_list.append(i.project_id)
    for i in pro_list:
        emp_list=[]
        emp_details=[]
        task_filter_byproid=task_filter_bydate.filter(project_id=i)
        fgh=Estimate.objects.get(projectname_id=i)
        billd_amount=fgh.billed_amount
        appr_amt=0
        for j in task_filter_byproid:
            appr_amt=appr_amt+j.amount
        client_var=Client.objects.get(id=i)
        for e in task_filter_byproid:
            if e.employee not in emp_list:
                emp_list.append(e.employee)
        for m in emp_list:
            task_filter_byemp_list=task_filter_byproid.filter(employee=m)
            work_hours=[]
            for i in task_filter_byemp_list:
                work_hours=work_hours=i.hours
            emp_details.append({"Employee_Name":m,"Hours_Spend":work_hours})
        emp_table=pd.DataFrame(emp_details)
        date_tod=date.today()
        html_content = render_to_string("client_automail.html",{'client_name':client_var.clientName,"emp_details":emp_details,"dayf":dayf,"dayt":dayt,"appr_amt":appr_amt,"date_tod":date_tod,"billd_amount":billd_amount})
        text_content=strip_tags(html_content)
        subject='Cydez Technologies'
        message='Hi  {0},your approved amount from {1} to {2} is Rs.{3} and billed amount as on {4} is Rs.{5}. \n '.format(client_var.clientName,dayf,dayt,appr_amt,date_tod,billd_amount)
        email_from=settings.EMAIL_HOST_USER
        recipient_list=["christojohnson96@gmail.com"]
        email = EmailMultiAlternatives(
            subject,
            text_content,
            email_from,
            recipient_list,
        )
        email.attach_alternative(html_content,"text/html")
        email.send()

#automatic task scheduled on friday 10am
def start():
    scheduler=apscheduler.schedulers.background.BackgroundScheduler()
    scheduler.add_job(auto_email_friday,'cron',day_of_week='fri', hour='10',minute='00')
    scheduler.start()


def listProject(request):
    l= Client.objects.all()
    activeprojects=list(Estimate.objects.filter(projectname__status="Active"))
    list1 = activeprojects
    completeprojects=list(Estimate.objects.filter(projectname__status="Completed"))
    list2 = completeprojects
    datas_list = list1+list2
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 30)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)

       
### for search project ###
    if request.method=='POST':
        s=request.POST.get('search')
        datas_list=Estimate.objects.filter(projectname__clientName__startswith=s)
        page = request.GET.get('page', 1)
        paginator = Paginator(datas_list, 20)
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            datas = paginator.page(1)
        except EmptyPage:
            datas = paginator.page(paginator.num_pages)
        return render(request,'estimate.html',{'datas':datas,'s':s})
    return render(request,'estimate.html',{'datas':datas,'l':l})

def deleteProject(request,projectid):
    k=Estimate.objects.get(id=projectid)
    k.delete()
    return redirect('listProject')

####client ###########
    
def create_client(request):
    if request.method == 'POST':
        clientName = request.POST.get('clientName')
        email = request.POST.get('email')
        optionalemail = request.POST.get('email1')
        status = request.POST.get('status')
        weeklyMail = request.POST.get('weeklymail')
        if weeklyMail == "on":
            weeklyMail = True
        else:
            weeklyMail=False
        parent =request.POST.get('parent')
        if parent!= "None":
            p=Engagement.objects.get(id=parent)
            c = Client.objects.create(weeklyMail=weeklyMail,clientName=clientName, email=email,optionalemail=optionalemail,status=status,Parent=p)
            Estimate.objects.create(projectname= c)
        else:
            c = Client.objects.create(weeklyMail=weeklyMail,clientName=clientName, email=email,optionalemail=optionalemail,status=status)
            Estimate.objects.create(projectname= c)
    return redirect('listclient')

def searchclient(request):
    datas=Client.objects.all()
    if request.method=='POST':
        s=request.POST.get('search')
        datas=datas.filter(clientName__contains=s)
        return render(request, 'client.html',{'datas':datas})
    
def listclient(request):
    parentproject=Engagement.objects.filter(status="Active")
    datas_list = Client.objects.all()
    query = request.GET.get('q')
    if query:
        datas_list = Client.objects.filter(clientName__contains=query)
    paginator = Paginator(datas_list, 50) # 6 datas per page
    page = request.GET.get('page',1)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request, "client.html",{'datas':datas,'parentproject':parentproject})
    

def editClient(request):
    if request.method=='POST':
        clientName=request.POST.get('clientName')
        client_id=request.POST.get('client_id')
        k=Client.objects.filter(id=client_id)
        email=request.POST.get('email')
        optionalemail = request.POST.get('email1')
        status=request.POST.get('status')
        weeklyMail = request.POST.get('weeklymail')
        if weeklyMail == "False":
            weeklyMail = True
        else:
            weeklyMail=False
        parent =request.POST.get('parent')
        if parent!= "None":
            p=Engagement.objects.get(engagementName=parent)
            k.update(weeklyMail=weeklyMail,clientName=clientName,email=email,optionalemail=optionalemail,status=status,Parent=p)
            return redirect('listclient')
        else:
            k.update(weeklyMail=weeklyMail,clientName=clientName,email=email,optionalemail=optionalemail,status=status)
            return redirect('listclient')
def deleteclient(request,id):
    u=Client.objects.get(id=id)
    u.delete()
    return redirect('listclient')

def accept(request,task_id):
    if task_id:
        bs = Task.objects.get(id=task_id)
        print(bs,"............bsbsbs")
        if request.GET.get('approve')=='true':            
            bs.status = True
            print(bs.status,"status")
            bs.save()
        elif request.GET.get('approve')=='false':
            bs.status = False
            print(bs.status,"status")
            bs.save()
    l=Client.objects.all()
    k=User.objects.all()
    datas_list=Task.objects.all().order_by('-date')
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,"totalbillingsearch.html",{'datas':datas,'l':l,'k':k,'task_id':task_id})
@csrf_exempt
def approve_all(request):
    today = date.today()
    d=today.day
    dd = today - timedelta(days = 2) 
    tk = Task.objects.filter(status=None).filter(date__gte= dd)
    if tk:
        for i in tk:
            i.status = True
            project = i.project
            if hasattr(project,'estimate'):
                estimate = i.project.estimate
                estimate.billed_amount += i.amount
                estimate.save()
            i.save()
    return redirect(Totalbilling)
    
def pending(request):
    today = date.today()
    d=today.strftime("%A")
    dd = today - timedelta(days = 2) 
    ddd=dd.strftime("%A")
    print(type(dd),"dddddd")
    k = Task.objects.filter(date__lte=today,date__gte= dd).values_list("employee")
    print("=======================================k========================",k)
    datas_list = User.objects.exclude(id__in=k) 
    print("---------------------------datalist------------------------",datas_list)   
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if request.method=='POST':
        s=request.POST.get('search')
        datas_list=User.objects.filter(first_name__contains=s)
        page = request.GET.get('page', 1)
        paginator = Paginator(datas_list, 20)
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            datas = paginator.page(1)
        except EmptyPage:
            datas = paginator.page(paginator.num_pages)
        return render(request,"pending_task.html",{'datas':datas,'k':k,'datas_list':datas_list,'s':s,'today':today,'dd':dd,'ddd':ddd,'d':d})
    return render(request,'pending_task.html',{'datas':datas,'k':k,'datas_list':datas_list,'today':today,'dd':dd,'ddd':ddd,'d':d})



@csrf_exempt
def Totalbilling(request):
    df=pd.DataFrame(Task.objects.all().values())
    l=Client.objects.filter(status="Active")
    k=User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    datas_list=Task.objects.all()
    if request.method == 'POST':
        task_id = request.POST.get('taskId')
        status = int(request.POST.get('status'))
        bs = Task.objects.get(id=task_id)
        if status:
            if not bs.status:
                bs.status = True
                if bs.amount:
                    project = bs.project
                    if hasattr(project,'estimate'):
                        estimate = bs.project.estimate
                        estimate.billed_amount += bs.amount
                        engagement=bs.project.Parent
                        if engagement!=None:
                            engagements=Engagement.objects.get(engagementName=engagement)
                            engagements.engagementValue += bs.amount
                            engagements.save()
                            user=User.objects.filter(engagement=engagement).values()
                            for i in user:
                                k=i['id']
                                user=User.objects.get(id=k)
                                user.value += bs.amount
                                user.save()
                        estimate.save()
        else:
            if bs.status == True:
                bs.status = False
                if bs.amount:
                    project = bs.project
                    if hasattr(project,'estimate'):
                        estimate = bs.project.estimate
                        estimate.billed_amount -= bs.amount 
                        engagement=bs.project.Parent
                        if engagement!=None:
                            engagements=Engagement.objects.get(engagementName=engagement)
                            engagements.engagementValue -= bs.amount
                            engagements.save()
                            user=User.objects.filter(engagement=engagement).values()
                            for i in user:
                                k=i['id']
                                user=User.objects.get(id=k)
                                user.value -= bs.amount
                                user.save()
                        estimate.save()
        bs.save()
        return JsonResponse({"status":True,"message":"success","button":status})
    l=Client.objects.filter(status="Active")
    k=User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    datas_list=Task.objects.all().order_by('-id')
    s = request.GET.get('status')
    p=request.GET.get('name')
    e=request.GET.get('employee')
    a=request.GET.get('start_date')
    b=request.GET.get('end_date')
    path=os.path.join(BASE_DIR,'media/csv/billing.xlsx')
    loop_count = 1
    writer = pd.ExcelWriter(path, engine='xlsxwriter')
    per_total = 0
    total_amount = 0
    employees={}
    search_variables = {}
    if s:
   
        search_variables['status__contains'] = s
    if p:
    
        search_variables['project__id'] = p
    if e:

        search_variables['employee__id'] = e
    if a:
        
        search_variables['date__lte'] = a
    if b:
       
        search_variables['date__gte'] = b
    for i in User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True):
        task = i.task_set.filter(**search_variables).values('employee__first_name','date','day','project__clientName','taskdetails','hours','amount')
        if task:
            for j in task:
                per_total += j['amount']
                o=per_total
            per_total=0
            
            df=pd.DataFrame(task)
            df2 = {'employee__first_name':'','date':'','day':'','project__clientName':'','taskdetails':'','hours':'Total','amount':o}
            df = df.append(df2, ignore_index = True)
            df.columns=['Employee','Date','Day','Project','Taskdetails','Hours','Amount']
            df['Date'] = pd.to_datetime(df.Date)
            df['Date'] = df['Date'].dt.strftime('%d-%m-%Y')
            
            loop_count += 1
            df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)
            ### for adjust width ###
            worksheet=writer.sheets['Sheet' + str(loop_count)]
            for idx,col in enumerate(df):
                series=df[col]
                max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
                worksheet.set_column(idx,idx,max_len)
            employees[i.get_full_name()]=[o]
            total_amount += o
    employees['Total']=[total_amount]
    new_df = pd.DataFrame(employees)
    new_df.to_excel(writer, sheet_name='Sheet1',index=False)
    writer.save()
    datas_list=Task.objects.filter(**search_variables).order_by('-id')
    
    page = request.GET.get('page', 1)
    
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if p:
        p=int(p)
    if e:
        e=int(e)
    return render(request,"totalbillingsearch.html",{'datas':datas,'l':l,'k':k,'s':s,'p':p,'e':e,'startdate':a,'enddate':b,'search_variables':search_variables})
## payment ##
def payment(request,clientid):
    # z=Payment.objects.all()
    client=Client.objects.get(id=clientid)
    print(client,client.id,"client")
    datas_list=Payment.objects.filter(name=clientid)
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'payment.html',{'datas':datas,'id':clientid,'client':client})
def addPayment(request):
    if request.method=='POST':
        clientName=request.POST.get('clientid')
        name=Client.objects.get(id=clientName)
        date=request.POST.get('date')
        details=request.POST.get('details')
        amountrecieved=request.POST.get('amountrecieved')
        Payment.objects.create(name=name,date=date,details=details,amountrecieved=amountrecieved)
        return redirect('payment',clientid=name.id)
    return render(request,'payment.html')
def deletePayment(request,paymentid):
    p=Payment.objects.all()
    k=Payment.objects.get(id=paymentid)
    clientid=k.name.id
    k.delete()
    return redirect('payment',clientid=clientid)
    #return render(request,'payment.html')
def editPayment(request):
    if request.method=='POST':
        clientName=request.POST.get('clientid')
        name=Client.objects.get(id=clientName)
        date=request.POST.get('date')
        details=request.POST.get('details')
        amountrecieved=request.POST.get('amountrecieved')
        user_id=request.POST.get('user_id')
        k=Payment.objects.filter(id=user_id)
        k.update(name=name,date=date,details=details,amountrecieved=amountrecieved)
        return redirect('payment',clientid=name.id)
    return render(request,'payment.html')
### user billing ###
def BillingPerfom(request,userid):
    h=0
    today = date.today().strftime("%Y-%m")
    r = date.today()
    start_date=date(int(r.year),int(r.month),1)
    k=User.objects.get(id=userid)
    print("NAME",k)
    j=User.objects.filter(id=userid)
    for i in j:
        d=i.date_joined
        s=i.salary
        b=i.billing
        total=int(s)*int(b)
        per_day=total/25
        per_hour=per_day/8
        task=i.task_set.filter(status=True,date__gte=start_date,date__lte=r)
        if task:
            for x in task:
                h +=x.hours
                total_hour=h
            h=0
        else:
            value=0
            Totalvalue=0
            messege="sorry you have no approved task in this month"
            return render(request,'justgage.html',{'messege':messege,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
    total_days_in_a_month=25
    total_working_hour_in_a_month=25*8
    numerator=per_hour*total_hour*100
    denominatior=per_hour* total_working_hour_in_a_month          
    value=numerator/denominatior
    print("VALUE",value)
### for filter by date ###
    if request.method=='POST':
        a=request.POST.get('date')
        print('date',a)
        # r=parse_date(a)
        # start_date=date(int(r.year),int(r.month),1)
        y=a.split('-')[0]
        m=a.split('-')[1]
        year=int(y)
        month=int(m)
        num_days=(calendar.monthrange(year,month)[1]);
        end_date=date(int(y),int(m),num_days)
        start_date=date(int(y),int(m),1)
        for i in j:
            d=i.date_joined
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=start_date,date__lte=end_date)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                value=0
                Totalvalue=0
                messege="sorry you have no approved task in this month"
                return render(request,'justgage.html',{'messege':messege,'k':k,'value':value,'id':userid,'Totalvalue':Totalvalue,'searchDate':a})
        total_days_in_a_month=25
        total_working_hour_in_a_month=25*8
        numerator=per_hour*total_hour*100
        denominatior=per_hour* total_working_hour_in_a_month          
        value=numerator/denominatior
        print(" value",value)
        current_date=date.today()
        end_date=date(int(current_date.year),int(current_date.month),1)
        days_before=(end_date-timedelta(days=1))
        num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
        for i in j:
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                Totalvalue=0
                return render(request,'justgage.html',{'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':a})
        numerator=per_hour*total_hour*100
        denominatior=per_hour*num_months*200
        Totalvalue=numerator/denominatior
        print("Totalvalue",Totalvalue)
        return render(request,'justgage.html',{'k':k,'value':value,'id':userid,'searchDate':a,'Totalvalue':Totalvalue})
### for total performance ###
    current_date=date.today()
    end_date=date(int(current_date.year),int(current_date.month),1)
    days_before=(end_date-timedelta(days=1))
    num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
    for i in j:
        s=i.salary
        b=i.billing
        total=int(s)*int(b)
        per_day=total/25
        per_hour=per_day/8
        task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
        if task:
            for x in task:
                h +=x.hours
                total_hour=h
            h=0
        else:
            Totalvalue=0
            return render(request,'justgage.html',{'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
    numerator=per_hour*total_hour*100
    denominatior=per_hour*num_months*200
    Totalvalue=numerator/denominatior
    print("Totalvalue",Totalvalue)
    return render(request,'justgage.html',{'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
    
def Incentive(request):
    first_day_of_current_month = date.today().replace(day=1)
    last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
    b=last_day_of_previous_month.strftime('%Y-%m')
    mon=last_day_of_previous_month.strftime('%B')
    year=last_day_of_previous_month.year
    k= Incentives.objects.filter(month=mon,year=year)
    if request.method =='POST':
        da = request.POST.get('searchdate')
        y = da.split('-')[0]
        m = da.split('-')[1]
        mo=date(int(y),int(m),1).strftime('%B')
        k=Incentives.objects.filter(month=mo,year=y)
        return render(request,'incentive.html',{'k':k,'selectedDate':da})
    return render(request,'incentive.html',{'k':k,'selectedDate':b})

def monthly(request):
    salary=0
    amounts=0
    first_day_of_current_month = date.today().replace(day=1)
    b=first_day_of_current_month.strftime('%Y-%m')
    y=b.split('-')[0]
    m=b.split('-')[1]
    year=int(y)
    month=int(m)
    num_days=(calendar.monthrange(year,month)[1])
    end_date=date(int(y),int(m),num_days)
    e = Expenditure.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    k = Payment.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    u = User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    for i in u:
        salary += i.salary
    expense = salary
    for o in e:
        expense += o.amount
    for j in k:
        amounts += j.amountrecieved
    if request.method =='POST':
        amounts=0
        a=request.POST.get('date')
        y=a.split('-')[0]
        m=a.split('-')[1]
        year=int(y)
        month=int(m)
        num_days=(calendar.monthrange(year,month)[1])
        end_date=date(int(y),int(m),num_days)
        start_date=date(int(y),int(m),1)
        e = Expenditure.objects.filter(date__gte=start_date,date__lte=end_date)
        k = Payment.objects.filter(date__gte=start_date,date__lte=end_date)
        expense = salary
        for p in e:
            expense += p.amount
        for i in k:
            amounts += i.amountrecieved
        return render(request,'monthly.html',{'k':k,'searchDate':a,'e':e,'amounts':amounts,'salary':salary,'expense':expense,'b':b})
    return render(request,'monthly.html',{'k':k,'searchDate':b,'amounts':amounts,'e':e,'salary':salary,'expense':expense,'b':b})

def expenses(request):
    salary=0
    amounts=0
    first_day_of_current_month = date.today().replace(day=1)
    b=first_day_of_current_month.strftime('%Y-%m')
    y=b.split('-')[0]
    m=b.split('-')[1]
    year=int(y)
    month=int(m)
    num_days=(calendar.monthrange(year,month)[1])
    end_date=date(int(y),int(m),num_days)
    e = Expenditure.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    k = Payment.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    u =User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    for i in u:
        salary += i.salary
    expense = salary
    for o in e:
        expense += o.amount
    for j in k:
        amounts += j.amountrecieved
    if request.method == 'POST':
        date_of_expense=request.POST.get('date')
        describe=request.POST.get('description')
        amounts=request.POST.get('amount')
        Expenditure.objects.create(date=date_of_expense,description=describe,amount=amounts)
        return redirect('monthly')
    return render(request,'monthly.html',{'e':e,'searchDate':b,'amounts':amounts,'salary':salary,'expense':expense,'b':b})

def delete_expense(request,expenseid):
    exp=Expenditure.objects.filter(id=expenseid)
    exp.delete()
    return redirect('monthly')
########### engagement ##########
def listEngagements(request):
    active=Engagement.objects.filter(status="Active")
    completed=Engagement.objects.filter(status="Completed")
    datas_list=active|completed
    query = request.GET.get('q')
    if query:
        datas_list = Engagement.objects.filter(engagementName__contains=query)
    paginator = Paginator(datas_list, 50) # 6 datas per page
    page = request.GET.get('page',1)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'engagement.html',{'datas':datas,})
def create_engagement(request):
    if request.method == 'POST':
        engagementName = request.POST.get('engagement')
        email = request.POST.get('email')
        status = request.POST.get('status')
        c = Engagement.objects.create(engagementName=engagementName,email=email,status=status)
    return redirect('listEngagements')
def editengagement(request):
    if request.method=='POST':
        engagementName = request.POST.get('engagement')
        email = request.POST.get('email')
        status = request.POST.get('status')
        engagement_id=request.POST.get('engagement_id')
        k=Engagement.objects.filter(id=engagement_id)
        k.update(engagementName=engagementName,email=email,status=status)
        return redirect('listEngagements')
def listconsultant(request):
    engagement =Engagement.objects.all().order_by('-id')
    activeusers=list(User.objects.filter(status="Active",role="Consultant").exclude(is_superuser=True))
    inactiveusers=list(User.objects.filter(status="Inactive",role="Consultant").exclude(is_superuser=True))
    datas_list=activeusers+inactiveusers
    query = request.GET.get('q')
    if query:
        datas_list = User.objects.filter(first_name__contains=query)
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'consultant.html',{'datas': datas,'engagement':engagement})
def addConsultant(request):
    datas=User.objects.filter(role="consultant").order_by('-id')
    engagement =Engagement.objects.all().order_by('-id')
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        billing=request.POST.get('billing')
        status=request.POST.get('status')
        role=request.POST.get('role')
        if User.objects.filter(phone=phone).exists():
            messege="Phone Number already exists"
            k={'status':status,"role":role,"name":name,'email':email,'billing':billing,}
            return render(request,'consultant.html',{'messege':messege,'userdetails':k,'datas':datas,'engagement':engagement})
        otp=random.randint(0000,9999)
        user=User.objects.create_user(status=status,role=role,first_name=name,phone=phone,email=email,billing=billing,otp=otp,password=str(otp))
        engagements=request.POST.getlist('engagement')
        engagements=Engagement.objects.filter(id__in=engagements).values_list('id',flat=True)
        user.engagement.set(engagements)
        user.save()
        subject='Cydez Technologies'
        message='Hi your username is {0}  and password is {1} and please login "http://sf.cydeztechnologies.com/" Thank you'.format(user.phone,user.otp)
        email_from=settings.EMAIL_HOST_USER
        officemail ="operations@cydeztechnologies.com"
        recipient_list=[user.email,officemail]
        send_mail(subject,message,email_from,recipient_list)
        return redirect('listconsultant')
    return render(request,'consultant.html',{'datas': datas,'engagement':engagement})
def editConsultant(request): 
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        user_id=request.POST.get('user_id')
        status=request.POST.get('status')
        role=request.POST.get('role')
        engagements=request.POST.getlist('engagement')
        if engagements!=[]:
            engagements=Engagement.objects.filter(engagementName__in=engagements).values_list('id',flat=True)
            pd=User.objects.get(id=user_id)
            k=pd.engagement.all().values_list('id',flat=True)
            if k:
                pd.engagement.remove(*k)
            pd.engagement.set(engagements)
            pd.save()
        j=User.objects.filter(id=user_id)
        j.update(status=status,role=role,first_name=name,phone=phone,email=email)
        return redirect('listconsultant')

@csrf_exempt
def consultatntbilling(request,userid):
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=Client.objects.filter(status="Active",Parent__in=engagement)
    emp=list(Task.objects.filter(project__in=l).values_list('employee_id',flat=True))
    k=User.objects.filter(id__in=emp).exclude(is_superuser=True)
    df=pd.DataFrame(Task.objects.filter(project__in=l).values())
    datas_list=Task.objects.filter(project__in=l)
    if request.method == 'POST':
        task_id = request.POST.get('taskId')
        status = int(request.POST.get('status'))
        bs = Task.objects.get(id=task_id)
        if status:
            if not bs.status:
                bs.status = True
                if bs.amount:
                    project = bs.project
                    if hasattr(project,'estimate'):
                        estimate = bs.project.estimate
                        estimate.billed_amount += bs.amount
                        engagement=bs.project.Parent
                        engagements=Engagement.objects.get(engagementName=engagement)
                        engagements.engagementValue += bs.amount
                        engagements.save()
                        user=User.objects.filter(engagement=engagement).values()
                        for i in user:
                            k=i['id']
                            user=User.objects.get(id=k)
                            user.value += bs.amount
                            user.save()
                        estimate.save()
                
        else:
            if bs.status == True:
                bs.status = False
                if bs.amount:
                    project = bs.project
                    if hasattr(project,'estimate'):
                        estimate = bs.project.estimate
                        estimate.billed_amount -= bs.amount
                        engagement=bs.project.Parent
                        engagements=Engagement.objects.get(engagementName=engagement)
                        engagements.engagementValue -= bs.amount
                        engagements.save()
                        user=User.objects.filter(engagement=engagement).values()
                        for i in user:
                            k=i['id']
                            user=User.objects.get(id=k)
                            user.value -= bs.amount
                            user.save()
                        estimate.save()             
        bs.save()
        return JsonResponse({"status":True,"message":"success","button":status})
    l=Client.objects.filter(status="Active",Parent__in=engagement)
    k=User.objects.filter(status="Active",id__in=emp).exclude(is_superuser=True)
    datas_list=Task.objects.filter(project__in=l)
    s = request.GET.get('status')
    p=request.GET.get('name')
    e=request.GET.get('employee')
    a=request.GET.get('start_date')
    b=request.GET.get('end_date')
    path=os.path.join(BASE_DIR,'media/csvfolder/billings.xlsx')
    loop_count = 1
    writer = pd.ExcelWriter(path, engine='xlsxwriter')
    per_total = 0
    total_amount = 0
    employees={}
    search_variables = {}
    if s:   
        search_variables['status__contains'] = s
    if p:
        search_variables['project__id'] = p
    if e:
        search_variables['employee__id'] = e
    if a:        
        search_variables['date__lte'] = a
    if b:
        search_variables['date__gte'] = b
    for i in User.objects.filter(status="Active",id__in=emp).exclude(is_superuser=True):
        task = i.task_set.filter(**search_variables).values('employee__first_name','date','day','project__clientName','taskdetails','hours','amount')
        if task:
            for j in task:
                per_total += j['amount']
                o=per_total
            per_total=0
            
            df=pd.DataFrame(task)
            df2 = {'employee__first_name':'','date':'','day':'','project__clientName':'','taskdetails':'','hours':'Total','amount':o}
            df = df.append(df2, ignore_index = True)
            df.columns=['Employee','Date','Day','Project','Taskdetails','Hours','Amount']
            df['Date'] = pd.to_datetime(df.Date)
            df['Date'] = df['Date'].dt.strftime('%d-%m-%Y')
            
            loop_count += 1
            df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)
            ### for adjust width ###
            worksheet=writer.sheets['Sheet' + str(loop_count)]
            for idx,col in enumerate(df):
                series=df[col]
                max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
                worksheet.set_column(idx,idx,max_len)
            employees[i.get_full_name()]=[o]
            total_amount += o
    employees['Total']=[total_amount]
    new_df = pd.DataFrame(employees)
    new_df.to_excel(writer, sheet_name='Sheet1',index=False)
    writer.save()
    if  search_variables == {}:
        datas_list=Task.objects.filter(project__in=l).order_by('-id')
    else:
        datas_list=Task.objects.filter(**search_variables).order_by('-id')
    page = request.GET.get('page', 1)    
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if p:
        p=int(p)
    if e:
        e=int(e)
    return render(request,"consultantsearch.html",{'datas':datas,'l':l,'k':k,'s':s,'p':p,'e':e,'startdate':a,'enddate':b,'search_variables':search_variables,'userid':userid})
@csrf_exempt
def approve_allConsultatnt(request,userid):
    today = date.today()
    d=today.day
    dd = today - timedelta(days = 2)
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=Client.objects.filter(status="Active",Parent__in=engagement)
    tk=Task.objects.filter(project__in=l,status=None).filter(date__gte= dd)
    if tk:
        for i in tk:
            i.status = True
            project = i.project
            if hasattr(project,'estimate'):
                estimate = i.project.estimate
                estimate.billed_amount += i.amount
                estimate.save()
            i.save()
    return redirect('consultatntbilling',userid=userid)
def pendingc(request,userid):
    userid=userid
    today = date.today()
    d=today.strftime("%A")
    dd = today - timedelta(days = 2) 
    ddd=dd.strftime("%A")
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=Client.objects.filter(status="Active",Parent__in=engagement)
    emp=list(Task.objects.filter(project__in=l).values_list('employee_id',flat=True))
    u=list(User.objects.filter(id__in=emp).exclude(is_superuser=True).values_list("id",flat=True))
    k = list(Task.objects.filter(date__lte=today,date__gte= dd,project__in=l).values_list("employee",flat=True))
    res = list(set(u)^set(k))
    datas_list = User.objects.filter(id__in=res) 
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if request.method=='POST':
        s=request.POST.get('search')
        datas_list=User.objects.filter(first_name__contains=s)
        page = request.GET.get('page', 1)
        paginator = Paginator(datas_list, 20)
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            datas = paginator.page(1)
        except EmptyPage:
            datas = paginator.page(paginator.num_pages)
        return render(request,"pending_taskc.html",{'userid':userid,'datas':datas,'k':k,'datas_list':datas_list,'s':s,'today':today,'dd':dd,'ddd':ddd,'d':d})
    return render(request,'pending_taskc.html',{'userid':userid,'datas':datas,'k':k,'datas_list':datas_list,'today':today,'dd':dd,'ddd':ddd,'d':d})
def estimateconsultant(request,userid):
    userid=userid
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=list(Client.objects.filter(status="Active",Parent__in=engagement).values_list('id',flat=True))
    datas_list1=list(Estimate.objects.filter(projectname__in=l).values_list('id',flat=True))
    datas_list=Estimate.objects.filter(projectname__in=l)
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 30)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)       
### for search project ###
    if request.method=='POST':
        s=request.POST.get('search')
        if s:
            datas_list2=list(Estimate.objects.filter(projectname__clientName__startswith=s).values_list('id',flat=True))
            result =  any(elem in datas_list2  for elem in datas_list1)
            if result:
                datas_list=Estimate.objects.filter(projectname__clientName__startswith=s)
                page = request.GET.get('page', 1)
                paginator = Paginator(datas_list, 20)
                try:
                    datas = paginator.page(page)
                except PageNotAnInteger:
                    datas = paginator.page(1)
                except EmptyPage:
                    datas = paginator.page(paginator.num_pages)
                return render(request,'estimatec.html',{'userid':userid,'datas':datas,'s':s})
            else:
                j=[]
                datas_list=Estimate.objects.filter(projectname__clientName__in=j)
                page = request.GET.get('page', 1)
                paginator = Paginator(datas_list, 20)
                try:
                    datas = paginator.page(page)
                except PageNotAnInteger:
                    datas = paginator.page(1)
                except EmptyPage:
                    datas = paginator.page(paginator.num_pages)
                return render(request,'estimatec.html',{'userid':userid,'datas':datas,'s':s})
        else:
            datas_list=Estimate.objects.filter(id__in=l)
            page = request.GET.get('page', 1)
            paginator = Paginator(datas_list, 30)
            try:
                datas = paginator.page(page)
            except PageNotAnInteger:
                datas = paginator.page(1)
            except EmptyPage:
                datas = paginator.page(paginator.num_pages)  
            return render(request,'estimatec.html',{'userid':userid,'datas':datas,'l':l})
    return render(request,'estimatec.html',{'userid':userid,'datas':datas,'l':l})
def estimation(request,userid):
    estimations = Estimation.objects.filter(consultantid=userid)
    page = request.GET.get('page', 1)
    paginator = Paginator(estimations, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'est.html',{'userid':userid,'datas':datas})
def generateEstimate(request,userid):
    userid=userid
    if request.method == 'POST':
        reportName=request.POST.get('report')
        reportNumber=request.POST.get('count')
        customerName=request.POST.get('customer')
        date=request.POST.get('date')
        currency=request.POST.get('currency')
        expireyDate=request.POST.get('expiresDate')
        memo=request.POST.get('memo')
        consultantid=User.objects.get(id=userid)
        data=Estimation.objects.create(consultantid=consultantid,reportName=reportName,reportNumber=reportNumber,customerName=customerName,date=date,currency=currency,expireyDate=expireyDate,memo=memo)
        estimationid = Estimation.objects.get(id=data.id)
        product=request.POST.getlist('combination_id1[]')
        description=request.POST.getlist('combination_id2[]')
        quantity=request.POST.getlist('combination_id3[]')
        price=request.POST.getlist('combination_id4[]')
        tax=request.POST.getlist('combination_id5[]')
        amounts=0
        amountss=[]
        L1=[]
        L1.append(product)
        L1.append(description)
        L1.append(quantity)
        L1.append(price)
        L1.append(tax)
        N = len(L1)
        j=0
        while j < len(product):
            res = [i[j] for i in L1[ : N]]
            pr=res[0]
            description=res[1]
            quan=res[2]
            prices=res[3]
            tax=res[4]
            tax = int(tax) /100
            amount= float(prices) * float(tax)
            amount=int(amount)
            amounts +=   amount
            amountss.append(amount)
            d=Productdetails.objects.create(estimationid=estimationid,product=pr,description=description,quantity=quan,price=prices,tax=tax,amount=amount)
            j += 1
        # write to pdf 
        a_dict = dict()
        i=0
        amt=[str(x) for x in amountss]
        while i < len(product):
            if 'product' in a_dict:
                a_dict['product'].append(product[i])
            else:
                a_dict['product'] = [product[i]]
            if 'quantity' in a_dict:
                a_dict['quantity'].append(quantity[i])
            else:
                a_dict['quantity'] = [quantity[i]]
            if 'price' in a_dict:
                a_dict['price'].append(price[i])
            else:
                a_dict['price'] = [price[i]]
            if 'amountss' in a_dict:
                amountss=str(amountss)
                a_dict['amountss'].append(amt[i])
            else:
                amountss=str(amountss)
                a_dict['amountss'] = [amt[i]]
            i +=1
        k=dict()
        k['reportNumber']=reportNumber
        k['date']=date_convert(date)
        k['expireyDate']=date_convert(expireyDate)
        amounts=str(amounts)
        k['amounts'] = amounts
        s=printoncertificate(data.id,k,a_dict)
        topdf(s.id)

        estimations = Productdetails.objects.filter(estimationid=estimationid)
        return render(request,'invoice.html',{'id':data.id,'certificateImage':s.image.url,'userid':userid,'data':estimations,'customerName':customerName,'reportNumber':reportNumber,'date':date,'expireyDate':expireyDate})
    return render(request,'estimatebilling.html',{'userid':userid})
def invoice(request,id):
    userid=id
    # estimationid = Estimation.objects.filter(id=id)
    # estimations = Productdetails.objects.filter(estimationid__id=estimationid[0].id)
    s = Estimation.objects.get(id=id)
    return render(request,'invoices.html',{'id':s.id,'certificateImage':s.image.url})

def GeneratePDF(request,id):
    s = Estimation.objects.get(id=id)
    path=s.pdf.path
    content_type = mimetypes.guess_type(path)
    filename=s.filename('pdf')   
   
    dwnfile=open(path,'rb')
    response= HttpResponse(dwnfile,content_type=content_type[0]) 
    response['Content-Disposition'] = "attachment; filename=%s"%filename

    return response
def estimation_admin(request):
    estimations = Estimation.objects.all()
    query = request.GET.get('q')
    if query:
        datas_list = User.objects.filter(first_name__contains=query)
    page = request.GET.get('page', 1)
    paginator = Paginator(estimations, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'esti.html',{'datas':datas})

