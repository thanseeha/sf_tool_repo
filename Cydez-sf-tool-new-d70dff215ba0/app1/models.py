from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password
from django.conf import settings
import os

# Create your models here.
def upload_path(instance,filename):
    if instance.customerName:
        folder=instance.reportNumber
    return 'students/{0}/{1}'.format(folder,filename)
class Engagement(models.Model):
    engagementName = models.CharField(default=" ",max_length=100,null=True,blank=True)
    engagementValue=models.IntegerField(default=0)
    email = models.EmailField(null=True,blank=True)
    status = models.CharField(max_length=50,null=True)
 
    class Meta:
        ordering =['engagementName']
    def __str__(self):
        return str(self.engagementName)
        
class UserManager(BaseUserManager):
       
    def _create_user(self, phone, password, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not phone:
            raise ValueError('Valid Mobile number must be given')
       # email = self.normalize_email(email)
        
        user = self.model(phone=phone, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def _create_user_phone(self, phone, password,otp, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not phone:
            raise ValueError('Phone number is mandatory')
        
        user = self.model(phone=phone,password=password,otp=otp, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, phone, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **other_fields)
    
    def create_user_phone(self, phone, password,otp, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user_phone(phone, password,otp,**other_fields)

    def create_superuser(self, phone, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **other_fields)

class User(AbstractUser):
    STATUS = (
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
    )
    roles=(
        ('Admin','Admin'),
        ('Consultant','Consultant'),
        ('Employee','Employee'),
        
    )
    username=models.CharField(max_length=100,null=True,blank=True)
    first_name=models.CharField(max_length=100,null=True,blank=True)
    email = models.EmailField(max_length=50, null=True,blank=True)
    phone = models.IntegerField(unique=True)
    salary = models.IntegerField(null=True,)
    billing = models.IntegerField(null=True,)
    amount=models.IntegerField(null=True)
    otp=models.IntegerField(null=True,default=None)
    desigenation=models.CharField(max_length=100,null=True,blank=True)
    rank=models.IntegerField(null=True)
    officemail=models.EmailField(max_length=50, null=True,blank=True)
    date=models.DateTimeField(auto_now_add=True,null=True,blank=True)
    status=models.CharField(max_length=50,choices = STATUS,null=True,blank=True,default="Active")
    role=models.CharField(max_length=50,choices = roles,null=True,blank=True)
    engagement = models.ManyToManyField('Engagement')
    value=models.IntegerField(default=0)
    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['email','otp','salary','billing','desigenation','rank','officemail','date','status','role']

    objects=UserManager()
    class Meta:
        ordering =['status']
    def __str__(self):
        return str(self.first_name)

      
class Client(models.Model):
    # STATUS = (
    #     ('Active', 'Active'),
    #     ('Completed', 'Completed'))
    Parent=models.ForeignKey('Engagement',on_delete=models.SET_NULL, blank=True, null=True)
    clientName = models.CharField(default=" ",max_length=100,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    optionalemail=models.EmailField(null=True,blank=True)
    phone = models.IntegerField(null=True,blank=True)
    status = models.CharField(max_length=50,null=True)
    weeklyMail = models.BooleanField(default=False)
 
    class Meta:
        ordering =['status']
    def __str__(self):
        return str(self.clientName)
      
class Estimate(models.Model):
    projectname=models.OneToOneField(Client,on_delete=models.CASCADE,null=True,blank=True)
    estimatedamount=models.IntegerField(null=True,default=None)
    paidamount=models.IntegerField(null=True,default=None)
    dueamount=models.IntegerField(null=True,default=None)
    billed_amount=models.IntegerField(null=True,default=0)
    def __str__(self):
        return str(self.projectname.status)

class Totalbilling(models.Model):
    date = models.DateField(null=True,blank=True)
    day = models.CharField(default=" ",max_length=200,null=True,blank=True)
    project = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    hours = models.FloatField(null=True,blank=True)
    def __str__(self):
        return str(self.id)
class Payment(models.Model):
    name=models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    date=models.DateField(null=True,blank=True)
    details=models.CharField(default=" ",max_length=200,null=True,blank=True)
    amountrecieved=models.IntegerField(null=True,default=None)
    def __str__(self):
        return str(self.id)
class Incentives(models.Model):
    employee = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    incentiveAmount = models.IntegerField(null=True,default=None)
    percentage =models.IntegerField(null=True,default=None)
    month =models.CharField(default=" ",max_length=200,null=True,blank=True)
    year=models.IntegerField(null=True,default=None)
    def __str__(self):
        return str(self.employee)
class Expenditure(models.Model):
    date = models.DateField(null=True,blank=True)
    description=models.CharField(default=" ",max_length=200,null=True,blank=True)
    amount=models.IntegerField(null=True,default=None)
    def __str__(self):
        return str(self.date)
class Estimation(models.Model):
    consultantid  = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    reportName = models.CharField(max_length=30,null=True,blank=True)
    reportNumber = models.IntegerField(null=True,blank=True)
    customerName = models.CharField(max_length=30,null=True,blank=True)
    date = models.DateField(null=True,blank=True)
    currency = models.CharField(max_length=30,null=True,blank=True)
    expireyDate = models.DateField(null=True,blank=True)
    memo = models.TextField(max_length=110,null=True,blank=True)

    image=models.ImageField(upload_to=upload_path,null=True,blank=True)
    pdf=models.FileField(upload_to=upload_path,null=True,blank=True)

    def __str__(self):
        return str(self.id)

    def filename(self,fname):
        if fname=='image':
            return os.path.basename(self.image.name)
        elif fname=='pdf':
            return os.path.basename(self.pdf.name)
        else:
            return os.path.basename(self.studentpic.name)
class Productdetails(models.Model):
    estimationid  = models.ForeignKey(Estimation,on_delete=models.CASCADE,null=True,blank=True)
    product = models.CharField(max_length=30,null=True,blank=True)
    description = models.TextField(max_length=110,null=True,blank=True)
    quantity = models.IntegerField(null=True,blank=True)
    price = models.IntegerField(null=True,blank=True)
    tax = models.IntegerField(null=True,blank=True)
    amount = models.IntegerField(null=True,blank=True)
    def __str__(self):
        return str(self.estimationid)
class Certificate(models.Model):
    name=models.CharField(max_length=50)
    image=models.ImageField(upload_to='certificates/')
    xvalue=models.IntegerField(default=0)
    yvalue=models.IntegerField(default=0)
    ixvalue=models.IntegerField(default=0)
    iyvalue=models.IntegerField(default=0)

    def __str__(self):
        return self.name
